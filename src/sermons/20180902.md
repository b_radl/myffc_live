---
lunr: "true"
title: "Righteousness - part 3"
author: "Pastor Frank Jones"
postDate: "09-02-2018"
date: 2018-09-02
category: "sermons"
slug: "2018/09/02"
icon: microphone
audioLink: "ffc_0902208"
tags: [ righteousness, 2 sides, Jesus ]
mp3: "ffc_0902208/09022018.mp3"
ogg: "ffc_0902208/09022018.ogg"
linkurl: "https://archive.org/download/ffc_0902208/09022018_files.xml"
ipath: "https://archive.org/download/ffc_0902208/09022018.mp3"
enclosure: ["https://archive.org/download/ffc_0902208/09022018.mp3"]
layout: sermon.html
---

Teaching from Pastor Frank Jones, Faith Family Church, Taylors, SC.
