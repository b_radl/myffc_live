---
lunr: "true"
title: "Blood Covenant"
author: "Pastor Frank Jones"
postDate: "11-11-2018"
date: 2018-11-11
category: "sermons"
slug: "2018/11/11"
icon: microphone
audioLink: "ffc_11112018"
tags: [ covenant, word ]
mp3: "ffc_11112018/11112018.mp3"
ogg: "ffc_11112018/11112018.ogg"
linkurl: "https://archive.org/download/ffc_11112018/11112018_files.xml"
ipath: "https://archive.org/download/ffc_11112018/11112018.mp3"
enclosure: ["https://archive.org/download/ffc_11112018/11112018.mp3"]
layout: sermon.html
---

Teaching from Pastor Frank Jones, Faith Family Church, Taylors, SC. 
