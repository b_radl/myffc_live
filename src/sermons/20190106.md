---
lunr: "true"
title: "Prayer of Faith"
author: "Pastor Frank Jones"
postDate: "01-06-2019"
date: 2019-01-06
category: "sermons"
slug: "2019/01/06"
icon: microphone
audioLink: "ffc_01062019"
tags: [ authority, standing ]
mp3: "ffc_01062019/01062019.mp3"
ogg: "ffc_01062019/01062019.ogg"
linkurl: "https://archive.org/download/ffc_01062019/01062019_files.xml"
ipath: "https://archive.org/download/ffc_01062019/01062019.mp3"
enclosure: ["https://archive.org/download/ffc_01062019/01062019.mp3"]
layout: sermon.html
---

Faith teaching from Pastor Frank Jones, Faith Family Church, Taylors, SC.
