---
lunr: "true"
title: "Secret to Success - Diligence"
author: "Pastor Brad Losh"
postDate: "03-03-2019"
date: 2019-03-03
category: "sermons"
slug: "2019/03/03"
icon: microphone
audioLink: "ffc_03032019"
tags: [ diligence, success ]
mp3: "ffc_03032019/03032019.mp3"
ogg: "ffc_03032019/03032019.ogg"
linkurl: "https://archive.org/download/ffc_03032019/03032019_files.xml"
ipath: "https://archive.org/download/ffc_03032019/03032019.mp3"
enclosure: ["https://archive.org/download/ffc_03032019/03032019.mp3"]
layout: sermon.html
---

Faith teaching from Pastor Brad Losh, Faith Family Church, Taylors, SC.
