---
lunr: "true"
title: "Resisting False Manhood"
author: "Pastor Frank Jones"
postDate: "03-31-2019"
date: 2019-03-31
category: "sermons"
slug: "2019/03/31"
icon: microphone
audioLink: "ffc_03312019"
tags: [ position ]
mp3: "ffc_03312019/03312019.mp3"
ogg: "ffc_03312019/03312019.ogg"
linkurl: "https://archive.org/download/ffc_03312019/03312019_files.xml"
ipath: "https://archive.org/download/ffc_03312019/03312019.mp3"
enclosure: ["https://archive.org/download/ffc_03312019/03312019.mp3"]
layout: sermon.html
---

Faith teaching from Pastor Frank Jones, Faith Family Church, Taylors, SC.
