---
lunr: "true"
title: "Mom's of Faith"
author: "Pastor Brad Losh"
postDate: "05-12-2019"
date: 2019-05-12
category: "sermons"
slug: "2019/05/12"
icon: microphone
audioLink: "ffc_05122019"
tags: [ mothers, obedience ]
mp3: "ffc_05122019/05122019.mp3"
ogg: "ffc_05122019/05122019.ogg"
linkurl: "https://archive.org/download/ffc_05122019/05122019_files.xml"
ipath: "https://archive.org/download/ffc_05122019/05122019.mp3"
enclosure: ["https://archive.org/download/ffc_05122019/05122019.mp3"]
layout: sermon.html
---

Mother's Day message from Pastor Brad Losh, Faith Family Church, Taylors, SC.
