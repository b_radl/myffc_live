---
lunr: "true"
title: "Relationships"
author: "Pastor Frank Jones"
postDate: "08-25-2019"
date: 2019-08-25
category: "sermons"
slug: "2019/08/25"
icon: microphone
audioLink: "ffc_08252019"
tags: [ dating, marriage, divorce ]
mp3: "ffc_08252019/08252019.mp3"
ogg: "ffc_08252019/08252019.ogg"
linkurl: "https://archive.org/download/ffc_08252019/08252019_files.xml"
ipath: "https://archive.org/download/ffc_08252019/08252019.mp3"
enclosure: ["https://archive.org/download/ffc_08252019/08252019.mp3"]
layout: sermon.html
---

Faith-filled teaching from Pastor Frank Jones, Faith Family Church, Taylors, SC.
