---
lunr: "true"
title: "Relationships - part 2"
author: "Pastor Frank Jones"
postDate: "09-01-2019"
date: 2019-09-01
category: "sermons"
slug: "2019/09/01"
icon: microphone
audioLink: "ffc_09012019"
tags: [ dating, marriage, divorce, re-marriage ]
mp3: "ffc_09012019/09012019.mp3"
ogg: "ffc_09012019/09012019.ogg"
linkurl: "https://archive.org/download/ffc_09012019/09012019_files.xml"
ipath: "https://archive.org/download/ffc_09012019/09012019.mp3"
enclosure: ["https://archive.org/download/ffc_09012019/09012019.mp3"]
layout: sermon.html
---

Faith-filled teaching from Pastor Frank Jones, Faith Family Church, Taylors, SC.
